# greypilgrim/starbound

This is a Docker image of the dedicated server for Starbound, using Steam.

It was originally sourced from [morgyn/starbound](https://github.com/Morgyn/docker-starbound).
That repo hadn't been updated in four years, so I built my own version.
The biggest difference is the image here is based on Debian Buster instead of
Ubuntu 16.04.

The difference between this Docker image and others is that you do not need
to store your Steam username, password, or disable Steamguard or save a
Steamguard key. The downside is **you need to manually update Starbound
within the container** when needed. This Docker image is not a good
choice for a VPS host. It is treated more like a mini virtual machine.
(But still 20x faster than setting up and maintaining a full virtual machine.)

## External info about the game

* http://playstarbound.com/
* http://store.steampowered.com/app/211820/

## Setup

### Run the image
```
docker run -d --name starbound \
-p 21025:21025 \
-v /host-path/starbound:/starbound \
--restart=unless-stopped \
greypilgrim/starbound:1.0
```

Replace `/host-path/starbound` with the host path you wish to store your
Starbound installation.

The image contains nothing but the update script and steamcmd. You will have to
first run update.sh to download the Starbound game into the container.

### Run the update script while the container is running
```
docker exec -it starbound /update.sh <steam login id>
```

The script will prompt you for your password, and if required Steamguard.
It will then perform the initial installation.

If it fails or quits for some reason, you can just re-run to complete.

**After successful completion of the installation, the container will restart**.

### Configure your Starbound server

Edit your configuration file in the installation path you chose, ie `/host-path/starbound`

https://starbounder.org/Guide:Setting_Up_Multiplayer#Advanced_Server_Configuration

## Updating Starbound

While the container is still running, execute the update script as above.
It will quit the server and then begin the update. Like the initial installation,
if the update fails or quits for some reason, just run it again. **It will restart
the container when the upgrade is successful** just like the first time.
