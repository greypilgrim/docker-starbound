FROM debian:buster-slim

MAINTAINER greypilgrim

ENV LANG="C.UTF-8" \
    STEAM_LOGIN="FALSE"

RUN apt-get update \
    && apt-get dist-upgrade -y \
    && apt-get autoremove -y

RUN apt-get install -q -y \
    ca-certificates \
    lib32gcc1 \
    libstdc++6 \
    wget \
    bsdtar

USER root

RUN mkdir -p /steamcmd

RUN mkdir -p /starbound

RUN cd /steamcmd \
	&& wget -o /tmp/steamcmd.tar.gz http://media.steampowered.com/installer/steamcmd_linux.tar.gz \
	&& tar zxvf steamcmd_linux.tar.gz \
	&& rm steamcmd_linux.tar.gz \
        && chmod +x ./steamcmd.sh

COPY start.sh /start.sh

COPY update.sh /update.sh

# Add initial require update flag
COPY .update /.update

WORKDIR /

EXPOSE 21025

ENTRYPOINT ["./start.sh"]
